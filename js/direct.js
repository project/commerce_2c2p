(function($, Drupal, drupalSettings) {
  "use strict";

  // Get the form ID.
  var formID = document
    .getElementsByClassName("commerce-order-item-add-to-cart-form")[0]
    .getAttribute("id");
  My2c2p.onSubmitForm(formID, function(errCode, errDesc) {
    let errorWrapper = document.getElementById("payment-errors");
    const message = new Drupal.Message(errorWrapper);
    message.clear();
    if (errCode != 0) {
      message.add(Drupal.t("Invalid credit card information."), { type: "error" });
    }
  });
})(jQuery, Drupal, drupalSettings);
