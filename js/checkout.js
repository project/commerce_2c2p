(function ($, Drupal, drupalSettings) {
  'use strict';

  var handlePaymentPostMessages = function handlePaymentPostMessages(_ref) {
    var data = _ref.data;
    var paymentResult = data.paymentResult;

    if (paymentResult) {
      var respCode = paymentResult.respCode,
          respDesc = paymentResult.respDesc,
          respData = paymentResult.respData;

      if (respCode === "1001") {
        window.location.replace(respData);
      }
      else {
        window.location.replace(drupalSettings.commerce_2c2p.return_url);
      }
    }
  };
  // Subscribe on post messages
  window.addEventListener('message', handlePaymentPostMessages);
}(jQuery, Drupal, drupalSettings));