<?php

namespace Drupal\commerce_2c2p\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_2c2p\Event\AlterPaymentParamsEvent;
use Drupal\commerce_2c2p\Event\Commerce2C2PPaymentEvents;
use Drupal\commerce_2c2p\Event\PaymentFailEvent;
use Drupal\commerce_2c2p\Event\PaymentSuccessEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Firebase\JWT\JWT;
use GuzzleHttp\ClientInterface;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A256GCM;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP;
use Jose\Component\Encryption\JWEBuilder;
use Jose\Component\Encryption\JWEDecrypter;
use Jose\Component\Encryption\Serializer\CompactSerializer;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\Algorithm\PS256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer as SignatureCompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "2c2p_direct",
 *   label = "2C2P Direct",
 *   display_label = "2C2P Direct",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_2c2p\PluginForm\Onsite\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_2c2p\PluginForm\PaymentMethodEditForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Direct extends OnsitePaymentGatewayBase implements SupportsNotificationsInterface, OffsitePaymentGatewayInterface, SupportsRefundsInterface {

  use LoggerChannelTrait;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * Constructs a new OffsiteRedirect2C2P object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, ClientInterface $http_client, MinorUnitsConverterInterface $minor_units_converter = NULL, EventDispatcherInterface $event_dispatcher, SerializerInterface $serializer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->httpClient = $http_client;
    $this->eventDispatcher = $event_dispatcher;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('http_client'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('event_dispatcher'),
      $container->get('serializer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'secret_key' => '',
      'certificate' => '',
      'private_key' => '',
      'private_key_pass' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['certificate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Certificate'),
      '#default_value' => $this->configuration['certificate'],
      '#required' => TRUE,
    ];

    $form['private_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Private key'),
      '#default_value' => $this->configuration['private_key'],
      '#required' => TRUE,
    ];

    $form['private_key_pass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key password'),
      '#default_value' => $this->configuration['private_key_pass'],
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['certificate'] = $values['certificate'];
      $this->configuration['private_key'] = $values['private_key'];
      $this->configuration['private_key_pass'] = $values['private_key_pass'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl() {
    if ($this->getMode() == 'test') {
      return 'https://sandbox-pgw.2c2p.com/payment/4.3/';
    }
    else {
      return 'https://pgw.2c2p.com/payment/4.3/';
    }
  }

  /**
   * Returns the payment actions URL.
   *
   * @return string
   *   The payment actions URL.
   */
  public function getPaymentActionsUrl() {
    if ($this->getMode() == 'test') {
      return 'https://demo2.2c2p.com/2C2PFrontend/PaymentAction/2.0/action';
    }
    else {
      return 'https://t.2c2p.com/PaymentAction/2.0/action';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl() {
    return Url::fromRoute('commerce_payment.notify', [
      'commerce_payment_gateway' => $this->parentEntity->id(),
    ], ['absolute' => TRUE]);
  }

  /**
   * Builds the URL to the "return" page.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\Core\Url
   *   The "return" page URL.
   */
  protected function buildReturnUrl($order) {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE, $tokenize = FALSE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    if (empty($payment_method)) {
      throw new \InvalidArgumentException('The provided payment has no payment method referenced.');
    }
    $order = $payment->getOrder();

    // Perform the create payment request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Remember to take into account $capture when performing the request.
    $invoice_no = time() . '-' . $order->id();
    $params = [
      'merchantID' => $this->configuration['merchant_id'],
      'invoiceNo' => $invoice_no,
      'description' => 'Payment',
      'amount' => str_pad($payment->getAmount()->getNumber(), 12, 0, STR_PAD_LEFT),
      'currencyCode' => $payment->getAmount()->getCurrencyCode(),
      'paymentChannel' => ['CC'],
      'backendReturnUrl' => $this->getNotifyUrl()->toString(),
      'frontendReturnUrl' => $this->buildReturnUrl($order)->toString(),
    ];

    $payment_method_token = $payment_method->getRemoteId();

    if ($tokenize) {
      $params['tokenize'] = TRUE;
    }
    else {
      $user = $order->getCustomer();
      $customer_token = $this->getRemoteCustomerId($user);
      $params['customerToken'] = [$customer_token];
      $params['request3DS'] = 'N';
    }

    /* Allow other modules to alter the parameters. */
    $event = new AlterPaymentParamsEvent($params, $order);
    $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::ALTER_PARAMS);
    $params = $event->getPaymentParams();

    $this->getLogger('commerce_2c2p')->debug('2C2P Payment Token Request: ' . serialize($params));

    $jwt = JWT::encode($params, $this->configuration['secret_key']);
    $pt_response = $this->httpClient->post($this->getApiUrl() . 'paymentToken', [
      'json' => [
        "payload" => $jwt,
      ],
    ]);

    $pt_res_data = json_decode($pt_response->getBody()->getContents(), TRUE);
    if (empty($pt_res_data['payload'])) {
      throw new PaymentGatewayException('Payment failed.');
    }
    $pt_res_payload = JWT::decode($pt_res_data['payload'], $this->configuration['secret_key'], [
      'HS256',
    ]);
    $this->getLogger('commerce_2c2p')->debug('2C2P Payment Token Response: ' . serialize($pt_res_payload));
    if ($pt_res_payload->respCode === "0000") {
      $order->setData('commerce_2c2p_payment_token', $pt_res_payload->paymentToken);
      $order->setData('commerce_2c2p_invoice_no', $invoice_no);
      $order->save();
      $do_payment_params = [
        'paymentToken' => $pt_res_payload->paymentToken,
        'payment' => [
          'code' => [
            'channelCode' => 'CC',
          ],
          'data' => [
            'email' => $order->getEmail(),
            'securePayToken' => $payment_method_token,
          ],
        ],
        'clientIP' => \Drupal::request()->getClientIp(),
      ];

      if ($tokenize) {
        $do_payment_params['payment']['data']['accountTokenization'] = TRUE;
      }
      else {
        $do_payment_params['payment']['data']['accountTokenization'] = FALSE;
        $do_payment_params['payment']['data']['customerToken'] = $customer_token;
      }

      $this->getLogger('commerce_2c2p')->debug('2C2P DoPayment Request: ' . serialize($do_payment_params));

      $do_pt_response = $this->httpClient->post($this->getApiUrl() . 'payment', [
        'json' => $do_payment_params,
      ]);

      $do_pt_res_data = json_decode($do_pt_response->getBody()->getContents(), TRUE);
      $this->getLogger('commerce_2c2p')->debug('2C2P DoPayment Response: ' . serialize($do_pt_res_data));
      switch ($do_pt_res_data['respCode']) {
        case '2000':
          // Check payment status on payment gateway.
          $inquiry_params = [
            "paymentToken" => $order->getData('commerce_2c2p_payment_token'),
            "merchantID" => $this->configuration['merchant_id'],
            "invoiceNo" => $order->getData('commerce_2c2p_invoice_no'),
          ];

          $inquiry_jwt = JWT::encode($inquiry_params, $this->configuration['secret_key']);
          $inquiry_pt_response = $this->httpClient->post($this->getApiUrl() . 'paymentInquiry', [
            'json' => [
              "payload" => $inquiry_jwt,
            ],
          ]);

          $inquiry_pt_res_data = json_decode($inquiry_pt_response->getBody()->getContents(), TRUE);
          $inquiry_pt_res_payload = JWT::decode($inquiry_pt_res_data['payload'], $this->configuration['secret_key'], [
            'HS256',
          ]);

          if ($inquiry_pt_res_payload->respCode === "0000") {
            $payment->setState('completed');
            $payment->setRemoteId($inquiry_pt_res_payload->tranRef);
            $payment->save();
            if ($tokenize) {
              $owner = $order->getCustomer();
              if ($owner->isAuthenticated()) {
                $this->setRemoteCustomerId($owner, $inquiry_pt_res_payload->customerToken);
                $owner->save();
              }
              else {
                $order->setData('commerce_2c2p_customer_token', $inquiry_pt_res_payload->customerToken);
                $order->save();
              }
            }
            // Trigger payment success event.
            $event = new PaymentSuccessEvent($inquiry_pt_res_payload, $order);
            $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::PAYMENT_SUCCESS);
          }
          else {
            // Trigger payment fail event.
            $event = new PaymentFailEvent($inquiry_pt_res_payload, $order);
            $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::PAYMENT_FAIL);
            throw new PaymentGatewayException($inquiry_pt_res_payload->respDesc);
          }
          break;

        case '1001':
          // Redirect needed, redirect to the url in "data" value.
          $response = new TrustedRedirectResponse($do_pt_res_data['data']);
          $response->send();
          break;

        default:
          // Trigger payment fail event.
          $event = new PaymentFailEvent($pt_res_payload, $order);
          $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::PAYMENT_FAIL);
          throw new PaymentGatewayException($do_pt_res_data['respDesc']);
      }
    }
    else {
      // Trigger payment fail event.
      $event = new PaymentFailEvent($pt_res_payload, $order);
      $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::PAYMENT_FAIL);
      throw new PaymentGatewayException($pt_res_payload->respDesc);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {}

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $payment_method->setRemoteId($payment_details['remote_id']);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {

  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $invoice_no = $order->getData('commerce_2c2p_invoice_no');

    // The key encryption algorithm manager with the RSAOAEP algorithm.
    $keyEncryptionAlgorithmManager = new AlgorithmManager([
      new RSAOAEP(),
    ]);
    // The content encryption algorithm manager with the A256CBC algorithm.
    $contentEncryptionAlgorithmManager = new AlgorithmManager([
      new A256GCM(),
    ]);
    // We instantiate our JWE Builder.
    $jweBuilder = new JWEBuilder(
      $keyEncryptionAlgorithmManager,
      $contentEncryptionAlgorithmManager,
    );
    try {
      $public_2c2p_key_enc = JWKFactory::createFromCertificate(
        $configuration['certificate'],
        [
          'use' => 'enc',
        ]
      );
      $serializer = new CompactSerializer();
      $private_merchant_key_sig = JWKFactory::createFromKey(
        $configuration['private_key'],
        $configuration['private_key_pass'] ?: NULL,
        [
          'use' => 'sig',
        ]
      );
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException('Payment refund failed, please review your payment gateway keys.');
    }
    $algorithmManager = new AlgorithmManager([
      new PS256(),
    ]);
    // We instantiate our JWS Builder.
    $jwsBuilder = new JWSBuilder($algorithmManager);
    $signature_serializer = new SignatureCompactSerializer();
    $jwsSerializerManager = new JWSSerializerManager([
      new SignatureCompactSerializer(),
    ]);
    // We instantiate our JWS Verifier.
    $jwsVerifier = new JWSVerifier(
      $algorithmManager
    );
    // We instantiate our JWE Decrypter.
    $jweDecrypter = new JWEDecrypter(
      $keyEncryptionAlgorithmManager,
      $contentEncryptionAlgorithmManager
    );
    // The serializer manager. We only use the JWE Compact Serialization Mode.
    $serializerManager = new JWESerializerManager([
      new CompactSerializer(),
    ]);
    $public_2c2p_key_sig = JWKFactory::createFromCertificate(
      $configuration['certificate'],
      [
        'use' => 'sig',
      ]
    );
    $private_merchant_key_enc = JWKFactory::createFromKey(
      $configuration['private_key'],
      $configuration['private_key_pass'] ?: NULL,
      [
        'use' => 'enc',
      ]
    );

    $inquiry_array = [
      'PaymentProcessRequest' => [
        'version' => '3.8',
        'merchantID' => $configuration['merchant_id'],
        'invoiceNo' => $invoice_no,
        'actionAmount' => str_pad($amount->getNumber(), 12, 0, STR_PAD_LEFT),
        'processType' => 'I',
      ],
    ];
    $inquiry_string = $this->serializer->encode($inquiry_array, 'xml');
    $inquiry_jwe = $jweBuilder
      ->create()
      ->withPayload($inquiry_string)
      ->withSharedProtectedHeader([
        'alg' => 'RSA-OAEP',
        'enc' => 'A256GCM',
      ])
      ->addRecipient($public_2c2p_key_enc)
      ->build();
    $inquiry_token = $serializer->serialize($inquiry_jwe, 0);
    $inquiry_jws = $jwsBuilder
      ->create()
      ->withPayload($inquiry_token)
      ->addSignature($private_merchant_key_sig, ['alg' => 'PS256'])
      ->build();
    $inquiry_token = $signature_serializer->serialize($inquiry_jws, 0);
    try {
      $inquiry_response = $this->httpClient->post($this->getPaymentActionsUrl(), [
        'body' => $inquiry_token,
      ]);
      $inquiry_res_body = $inquiry_response->getBody()->getContents();
      $inquiry_jws = $jwsSerializerManager->unserialize($inquiry_res_body);
      if (!$jwsVerifier->verifyWithKey($inquiry_jws, $public_2c2p_key_sig, 0)) {
        throw new PaymentGatewayException('Response JWS verification failed.');
      }
      $inquiry_jwe = $serializerManager->unserialize($inquiry_jws->getPayload());
      // We decrypt the token. This method does NOT check the header.
      if (!$jweDecrypter->decryptUsingKey($inquiry_jwe, $private_merchant_key_enc, recipient: 0)) {
        throw new PaymentGatewayException('Response JWE decryption failed.');
      }
      $inquiry_payload = $inquiry_jwe->getPayload();
      $inquiry_data = $this->serializer->decode($inquiry_payload, 'xml');
      switch ($inquiry_data['status']) {
        case 'A':
          $operation = 'V';
          break;

        case 'S':
          $operation = 'R';
          break;

        default:
          throw new PaymentGatewayException('Invalid payment status.');
      }
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException('Payment refund failed.');
    }

    $request_array = [
      'PaymentProcessRequest' => [
        'version' => '3.8',
        'merchantID' => $configuration['merchant_id'],
        'invoiceNo' => $invoice_no,
        'actionAmount' => str_pad($amount->getNumber(), 12, 0, STR_PAD_LEFT),
        'processType' => $operation,
      ],
    ];
    $request_string = $this->serializer->encode($request_array, 'xml');
    $jwe = $jweBuilder
      ->create()
      ->withPayload($request_string)
      ->withSharedProtectedHeader([
        'alg' => 'RSA-OAEP',
        'enc' => 'A256GCM',
      ])
      ->addRecipient($public_2c2p_key_enc)
      ->build();
    $token = $serializer->serialize($jwe, 0);
    $jws = $jwsBuilder
      ->create()
      ->withPayload($token)
      ->addSignature($private_merchant_key_sig, ['alg' => 'PS256'])
      ->build();
    $token = $signature_serializer->serialize($jws, 0);

    try {
      $pt_response = $this->httpClient->post($this->getPaymentActionsUrl(), [
        'body' => $token,
      ]);
      $pt_res_body = $pt_response->getBody()->getContents();
      $jws = $jwsSerializerManager->unserialize($pt_res_body);
      if (!$jwsVerifier->verifyWithKey($jws, $public_2c2p_key_sig, 0)) {
        throw new PaymentGatewayException('Response JWS verification failed.');
      }
      $jwe = $serializerManager->unserialize($jws->getPayload());
      // We decrypt the token. This method does NOT check the header.
      if (!$jweDecrypter->decryptUsingKey($jwe, $private_merchant_key_enc, recipient: 0)) {
        throw new PaymentGatewayException('Response JWE decryption failed.');
      }
      $payload = $jwe->getPayload();
      $data = $this->serializer->decode($payload, 'xml');
      if ($data['respCode'] === '00') {
        $old_refunded_amount = $payment->getRefundedAmount();
        $new_refunded_amount = $old_refunded_amount->add($amount);
        if ($new_refunded_amount->lessThan($payment->getAmount())) {
          $payment->setState('partially_refunded');
        }
        else {
          $payment->setState('refunded');
        }
        $payment->setRefundedAmount($new_refunded_amount);
        $payment->save();
      }
      else {
        throw new PaymentGatewayException('Payment refund failed.');
      }
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException('Payment refund failed.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    /* Check if order was completed in on notify, if so don't do anything. */
    if ($order->getState()->getId() !== 'completed') {
      $params = [
        "paymentToken" => $order->getData('commerce_2c2p_payment_token'),
        "merchantID" => $this->configuration['merchant_id'],
        "invoiceNo" => $order->getData('commerce_2c2p_invoice_no'),
      ];

      $jwt = JWT::encode($params, $this->configuration['secret_key']);
      $pt_response = $this->httpClient->post($this->getApiUrl() . 'paymentInquiry', [
        'json' => [
          "payload" => $jwt,
        ],
      ]);

      $pt_res_data = json_decode($pt_response->getBody()->getContents(), TRUE);
      $pt_res_payload = JWT::decode($pt_res_data['payload'], $this->configuration['secret_key'], [
        'HS256',
      ]);

      $this->getLogger('commerce_2c2p')->debug('2C2P paymentInquiry Response: ' . serialize($pt_res_payload));
      if ($pt_res_payload->respCode === "0000") {
        $payment_method = $order->get('payment_method')->entity;
        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
        $payment = $payment_storage->create([
          'state' => 'completed',
          'amount' => $order->getBalance(),
          'payment_gateway' => $this->parentEntity->id(),
          'payment_method' => $payment_method->id(),
          'order_id' => $order->id(),
          'remote_id' => $pt_res_payload->tranRef,
          'remote_state' => $pt_res_payload->respDesc,
          'card_type' => $pt_res_payload->paymentScheme,
        ]);
        $payment->save();

        $owner = $order->getCustomer();
        if ($owner->isAuthenticated()) {
          $this->setRemoteCustomerId($owner, $pt_res_payload->customerToken);
          $owner->save();
        }
        else {
          $order->setData('commerce_2c2p_customer_token', $pt_res_payload->customerToken);
          $order->save();
        }
        // Trigger payment success event.
        $event = new PaymentSuccessEvent($pt_res_payload, $order);
        $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::PAYMENT_SUCCESS);
      }
      else {
        // Trigger payment fail event.
        $event = new PaymentFailEvent($pt_res_payload, $order);
        $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::PAYMENT_FAIL);
        throw new PaymentGatewayException($pt_res_payload->respDesc);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $content = json_decode($request->getContent(), TRUE);
    $this->getLogger('commerce_2c2p')->debug('2C2P server-to-server Response: ' . serialize($content));
    if (!empty($content['payload'])) {
      try {
        $pt_res_payload = JWT::decode($content['payload'], $this->configuration['secret_key'], [
          'HS256',
        ]);
        $this->getLogger('commerce_2c2p')->debug('2C2P Payload Response notify ' . serialize($pt_res_payload));
        /** @var \Drupal\commerce_order\entity\OrderInterface $order */
        [, $order_id] = explode('-', $pt_res_payload->invoiceNo);
        $order = $this->entityTypeManager->getStorage('commerce_order')->loadForUpdate($order_id);
        if (!$order) {
          $this->getLogger('commerce_2c2p')->error('Invalid order ID from gateway: @reference', ['@reference' => $pt_res_payload->invoiceNo]);
          throw new PaymentGatewayException('Invalid order ID from gateway.');
        }
        if ($order->getState()->getId() !== 'completed') {
          if ($pt_res_payload->respCode === "0000") {
            $payment_method = $order->get('payment_method')->entity;
            $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
            $payment = $payment_storage->create([
              'state' => 'completed',
              'amount' => $order->getBalance(),
              'payment_gateway' => $this->parentEntity->id(),
              'payment_method' => $payment_method->id(),
              'order_id' => $order->id(),
              'remote_id' => $pt_res_payload->tranRef,
              'remote_state' => $pt_res_payload->respDesc,
            ]);
            $payment->save();

            $owner = $order->getCustomer();
            if ($owner->isAuthenticated()) {
              $this->setRemoteCustomerId($owner, $pt_res_payload->customerToken);
              $owner->save();
            }
            else {
              $order->setData('commerce_2c2p_customer_token', $pt_res_payload->customerToken);
              $order->save();
            }
            // Trigger payment success event.
            $event = new PaymentSuccessEvent($pt_res_payload, $order);
            $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::PAYMENT_SUCCESS);
          }
          else {
            throw new PaymentGatewayException($pt_res_payload->respDesc);
          }
        }
        else {
          // Order completed, do nothing.
          return new Response('', 200);
        }
      }
      catch (\UnexpectedValueException | PaymentGatewayException) {
        return new Response('', 200);
      }
      finally {
        if ($order ?? NULL) {
          $order->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

}
