<?php

namespace Drupal\commerce_2c2p\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\commerce_2c2p\Event\AlterPaymentParamsEvent;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_2c2p\Event\Commerce2C2PPaymentEvents;
use Firebase\JWT\JWT;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Form for 2C2P Iframe.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  use LoggerChannelTrait;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientInterface $http_client, EventDispatcherInterface $event_dispatcher) {
    $this->httpClient = $http_client;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $invoice_no = time() . '-' . $order->id();
    $pt_data_array = [
      // MANDATORY PARAMS.
      "merchantID" => $configuration['merchant_id'],
      "invoiceNo" => $invoice_no,
      "description" => 'Payment',
      "amount" => str_pad($payment->getAmount()->getNumber(), 12, 0, STR_PAD_LEFT),
      "currencyCode" => $payment->getAmount()->getCurrencyCode(),
      // OPTIONAL PARAMS.
      "frontendReturnUrl" => $form['#return_url'],
      "backendReturnUrl" => $payment_gateway_plugin->getNotifyUrl()->toString(),
      // MANDATORY RANDOMIZER.
      "nonceStr" => (string) rand(1000000000, 9999999999),
    ];

    /* Allow other modules to alter the parameters. */
    $event = new AlterPaymentParamsEvent($pt_data_array, $order);
    $this->eventDispatcher->dispatch($event, Commerce2C2PPaymentEvents::ALTER_PARAMS);
    $pt_data_array = $event->getPaymentParams();

    $jwt = JWT::encode($pt_data_array, $configuration['secret_key']);
    $pt_response = $this->httpClient->post($payment_gateway_plugin->getApiUrl() . 'PaymentToken', [
      'json' => [
        "payload" => $jwt,
      ],
    ]);

    $pt_res_data = json_decode($pt_response->getBody()->getContents(), TRUE);
    try {
      $pt_res_payload = JWT::decode($pt_res_data['payload'], $configuration['secret_key'], [
        'HS256',
      ]);
      $order->setData('commerce_2c2p_payment_token', $pt_res_payload->paymentToken);
      $order->setData('commerce_2c2p_invoice_no', $invoice_no);
      $order->save();
    }
    catch (\UnexpectedValueException $e) {
      $this->getLogger('commerce_2c2p')->debug('2C2P PaymentToken Response: ' . serialize($pt_res_data));
      $form['2c2p_iframe'] = [
        '#markup' => $this->t('Payment currently unavailable.'),
      ];
    }

    $options['src'] = $pt_res_payload->webPaymentUrl;
    $options['width'] = '100%';
    $options['height'] = 650;
    $options['frameborder'] = 0;
    $options['id'] = '2c2p_frame';

    $form['2c2p_iframe'] = [
      '#theme' => '2c2p_iframe',
      '#src' => $pt_res_payload->webPaymentUrl,
      '#attributes' => new Attribute($options),
    ];

    $form['#attached']['library'] = ['commerce_2c2p/checkout'];
    $form['#attached']['drupalSettings']['commerce_2c2p'] = [
      'return_url' => $form['#return_url'],
    ];

    return $form;
  }

}
