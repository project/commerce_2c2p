<?php

namespace Drupal\commerce_2c2p\PluginForm\Onsite;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the 2C2P onsite payment method add form.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $payment_gateway_configuration = $this->entity->getPaymentGateway()->getPluginConfiguration();
    $element = parent::buildCreditCardForm($element, $form_state);

    $element['number']['#attributes']['data-encrypt'] = 'cardnumber';
    $element['number']['#attributes']['required'] = 'required';
    $element['number']['#required'] = FALSE;

    $element['expiration']['month']['#attributes']['data-encrypt'] = 'month';
    $element['expiration']['year']['#attributes']['data-encrypt'] = 'year';

    $element['security_code']['#attributes']['data-encrypt'] = 'cvv';
    $element['security_code']['#attributes']['required'] = 'required';
    $element['security_code']['#required'] = FALSE;
    $element['#attached']['library'][] = 'commerce_2c2p/direct_' . $payment_gateway_configuration['mode'];

    // To display validation errors.
    $element['payment_errors'] = [
      '#type' => 'markup',
      '#markup' => '<div id="payment-errors"></div>',
      '#weight' => -200,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // We use input instead of values since 2c2p add the tokenized data there.
    $input = $form_state->getUserInput();
    if (empty($input['encryptedCardInfo']) || empty($input['maskedCardInfo']) || empty($input['expMonthCardInfo']) || empty($input['expYearCardInfo'])) {
      $this->logger->error('Invalid payment details.');
      return;
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    $card_type = CreditCard::detectType(str_replace('X', 0, $input['maskedCardInfo']));
    $this->entity->card_type = $card_type->getId();
    $this->entity->card_number = substr($input['maskedCardInfo'], -4);
    $this->entity->card_exp_month = $input['expMonthCardInfo'];
    $this->entity->card_exp_year = $input['expYearCardInfo'];
    $values = [
      'remote_id' => $input['encryptedCardInfo'],
    ];
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    // The payment method form is customer facing. For security reasons
    // the returned errors need to be more generic.
    try {
      $payment_gateway_plugin->createPaymentMethod($payment_method, $values);
    }
    catch (DeclineException $e) {
      $this->logger->warning($e->getMessage());
      throw new DeclineException(t('We encountered an error processing your payment method. Please verify your details and try again.'));
    }
    catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException(t('We encountered an unexpected error processing your payment method. Please try again later.'));
    }
  }

}
