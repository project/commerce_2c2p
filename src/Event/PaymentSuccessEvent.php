<?php

namespace Drupal\commerce_2c2p\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the event for altering the params extracted from the response.
 *
 * @see \Drupal\commerce_2c2p\Event\Commerce2C2PPaymentEvents
 */
class PaymentSuccessEvent extends Event {

  /**
   * The response payload.
   *
   * @var object
   */
  protected $payload;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Constructs a new AlterPaymentParamsEvent object.
   *
   * @param object $payload
   *   The response payload.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(\stdClass $payload, OrderInterface $order) {
    $this->payload = $payload;
    $this->order = $order;
  }

  /**
   * Gets the response payload.
   *
   * @return object
   *   The payload.
   */
  public function getResponsePayload() {
    return $this->payload;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder() {
    return $this->order;
  }

}
