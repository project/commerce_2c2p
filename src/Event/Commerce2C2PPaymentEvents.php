<?php

namespace Drupal\commerce_2c2p\Event;

/**
 * Defines constants for module specific event hooks.
 */
final class Commerce2C2PPaymentEvents {

  /**
   * Name of the event to allow amount modification before API call.
   *
   * @Event
   *
   * @see \Drupal\commerce_2c2p\Event\AlterPaymentParamsEvent
   */
  const ALTER_PARAMS = 'commerce_2c2p.alter_payment_params';

  /**
   * Event to allow order modification after payment gateway plugin request.
   *
   * @Event
   *
   * @see \Drupal\commerce_2c2p\Event\PaymentSuccessEvent
   */
  const PAYMENT_SUCCESS = 'commerce_2c2p.payment_success';

  /**
   * Event to allow order modification after payment gateway plugin request.
   *
   * @Event
   *
   * @see \Drupal\commerce_2c2p\Event\PaymentFailEvent
   */
  const PAYMENT_FAIL = 'commerce_2c2p.payment_fail';

}
