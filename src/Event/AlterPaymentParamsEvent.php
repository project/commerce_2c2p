<?php

namespace Drupal\commerce_2c2p\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the event for altering the parms sent to the API.
 *
 * @see \Drupal\commerce_2c2p\Event\Commerce2C2PPaymentEvents
 */
class AlterPaymentParamsEvent extends Event {

  /**
   * The prameters.
   *
   * @var array
   */
  protected $params;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Constructs a new AlterPaymentParamsEvent object.
   *
   * @param array $params
   *   The prameters.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(array $params, OrderInterface $order) {
    $this->params = $params;
    $this->order = $order;
  }

  /**
   * Gets the payment prameters.
   *
   * @return array
   *   The prameters.
   */
  public function getPaymentParams() {
    return $this->params;
  }

  /**
   * Sets the payment prameters.
   *
   * @param array $params
   *   The prameters.
   */
  public function setPaymentParams(array $params) {
    $this->params = $params;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder() {
    return $this->order;
  }

}
